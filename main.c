#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HEADER_SIZE 54
#define MAX_FILE_SIZE 1000

// function encode
void hideMessage(FILE *image, char *message)
{
    int i, j, k = 0, message_len = strlen(message);
    char byte, bit;

    // read header of the image file
    unsigned char header[HEADER_SIZE];
    fread(header, sizeof(unsigned char), HEADER_SIZE, image);

    // read the rest of the image file
    unsigned char *image_data = (unsigned char*) malloc(MAX_FILE_SIZE);
    int file_size = fread(image_data, sizeof(unsigned char), MAX_FILE_SIZE, image);

    // go through the message and hide each character in the image
    for (i = 0; i < message_len; i++)
    {
        byte = message[i];

        // go through each bit of the byte
        for (j = 0; j < 8; j++)
        {
            bit = (byte >> j) & 1;

            // modify the least significant bit of the image pixel
            image_data[k] = (image_data[k] & 0xFE) | bit;

            // move to the next pixel
            k++;

            // break if message is too long for the image
            if (k >= file_size)
            {
                printf("Image file too small to hide the entire message.\n");
                return;
            }
        }
    }

    // write the modified image data to a new image file
    FILE *output = fopen("output.bmp", "wb");
    fwrite(header, sizeof(unsigned char), HEADER_SIZE, output);
    fwrite(image_data, sizeof(unsigned char), file_size, output);
    fclose(output);

    printf("Message hidden successfully in output.bmp !\n");
}

// function decode
void retrieveMessage(FILE *image) {
    int i, j, k = 0;
    char byte = 0, bit;

    // read header of the image file
    unsigned char header[HEADER_SIZE];
    fread(header, sizeof(unsigned char), HEADER_SIZE, image);

    // read the rest of the image file
    unsigned char *image_data = (unsigned char*) malloc(MAX_FILE_SIZE);
    int file_size = fread(image_data, sizeof(unsigned char), MAX_FILE_SIZE, image);

    // go through the image pixels and retrieve the message
    for (i = 0; i < file_size; i++)
    {
        bit = image_data[i] & 1;
        byte = (byte << 1) | bit;

        // every 8 bits, add the byte to the message
        if ((i+1) % 8 == 0)
        {
            printf("%c", byte);
            byte = 0;
        }
    }

    printf("\n");
}

int main() {
    FILE *image;
    char message[1000];
    char input_image[1000];

    int option;
    printf("Usage :");
    printf("e. Hide message in image\n");
    printf("   -must enter input path(required)\n");
    printf("d. Retrieve message from image\n");
    printf("   -must enter input path(required)\n");
    printf("Choose an option(e or d) : ");
    scanf("%c", &option);

    if (option == 'e')
    {
        printf("Enter input image name/path : \n");
        scanf(" %[^\n]s", input_image);
        image = fopen(input_image, "rb");
        if(image == NULL)
        {
            printf("Failed to open this file!\n");
            return 1;
        }
        printf("Enter message to hide:\n");
        scanf(" %[^\n]s", message);
        hideMessage(image, message);
        fclose(image);
    }
    else if (option == 'd')
    {
        printf("Enter input image name/path : \n");
        scanf(" %[^\n]s", input_image);
        image = fopen(input_image, "rb");
        if(image == NULL)
        {
            printf("Failed to open this file!\n");
            return 1;
        }
        retrieveMessage(image);
        fclose(image);
    }
    else
    {
        printf("Invalid option!\n");
        return 1;
    }

    return 0;
}
